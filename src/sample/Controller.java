package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.fxml.FXMLLoader;

import java.io.*;

public class Controller {
    @FXML Menu fitxer;
    @FXML MenuItem sortir;
    @FXML Menu editar;
    @FXML MenuItem importar;
    @FXML MenuItem copiar;
    @FXML MenuItem  enganxar;
    @FXML MenuItem tallar;
    @FXML MenuItem desfer;
    @FXML TextArea textArea;
    @FXML MenuItem sobreEditor;
    @FXML RadioMenuItem Arial;
    @FXML RadioMenuItem TimesNew;
    @FXML RadioMenuItem Spectral;
    @FXML RadioMenuItem t8;
    @FXML RadioMenuItem t10;
    @FXML RadioMenuItem t12;
    @FXML RadioMenuItem  t30;
    @FXML MenuItem guardar;

    public String text = "";

    public void prova() {
        if (textArea.getSelectedText().length()==0 && text.isEmpty()){
            copiar.setDisable(true);
            enganxar.setDisable(true);

        }else {
            copiar.setDisable(false);
            enganxar.setDisable(false);
        }
    }
    public void copian(){
        text = textArea.getSelectedText();
        textArea.copy();


    }
    public void enganxant(){
        enganxar.setDisable(false);
        textArea.paste();
    }
    public void desfer(){
        text = "";

    }
    public void tallant(){
        textArea.cut();
    }

    public void importar(ActionEvent event) throws Exception{
        Stage st = (Stage)textArea.getScene().getWindow();

        FileChooser fileChooser = new FileChooser();
        File selectedfiled = fileChooser.showOpenDialog(st);
        if (selectedfiled != null){
            BufferedReader br = new BufferedReader(new FileReader(selectedfiled));
            String ste;
            String paste = "";
            while ((ste = br.readLine())!= null) {
                paste = paste + ste + "\n";
            }
            textArea.setText(paste);
            System.out.println(selectedfiled.getName());
        }

    }

    public void guardar()throws Exception{
        Stage st = (Stage)textArea.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        File selectedfiled2 = fileChooser.showOpenDialog(st);
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(selectedfiled2));
        bw.write(textArea.getText());
        bw.close();




    }

    public void exit() {
        Platform.exit();
    }

    public void info (){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sobre l'Editor");
        alert.setHeaderText(null);
        alert.setContentText("Amb aquest editor pots editar textos. \nCreat per Javier Blasco");

        alert.showAndWait();

    }

    public void Font (){

        String font = textArea.getFont().getFamily();
        System.out.println(font);
        if (Arial.isSelected()){
            textArea.setFont(Font.font ("System"));
        }else if (TimesNew.isSelected()){
            textArea.setFont(Font.font ("SansSerif"));
        }else if (Spectral.isSelected()){
            textArea.setFont(Font.font ("Gentium"));
        }

    }
    public void tamany(){
        double font = textArea.getFont().getSize();
        System.out.println(font);
        if (t8.isSelected()){
            textArea.setFont(Font.font (8));
        }else if (t10.isSelected()){
            textArea.setFont(Font.font (10));
        }else if (t12.isSelected()){
            textArea.setFont(Font.font (12));
        }else if (t30.isSelected()){
            textArea.setFont(Font.font (30));
        }
    }


}